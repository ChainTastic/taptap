package com.dinfogarneau.coursmobile.travaux_pratiques.taptap;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Image extends AppCompatActivity {

    TextView tvScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        tvScore = findViewById(R.id.tv_Score);

        Intent intent = getIntent();
        long score = intent.getLongExtra(Game.EXTRA_SCORE, 0);
        tvScore.setText(getString(R.string.txt_currentScore, score));
    }
}