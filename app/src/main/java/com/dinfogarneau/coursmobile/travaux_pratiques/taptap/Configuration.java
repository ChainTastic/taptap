package com.dinfogarneau.coursmobile.travaux_pratiques.taptap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Configuration extends AppCompatActivity implements View.OnClickListener {

    Button btnResetScore;
    TextView tvPseudo;
    EditText etPseudo;
    Button btnOK;
    Button btnPartager;
    Intent intent;
    String pseudo;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        bindViews();
        updateViews();
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE);
    }

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews() {
        btnResetScore = findViewById(R.id.btn_resetScore);
        tvPseudo = findViewById(R.id.tv_pseudo);
        etPseudo = findViewById(R.id.et_Pseudo);
        btnOK = findViewById(R.id.btn_OK);
        btnPartager = findViewById(R.id.btn_Partager);

        btnResetScore.setOnClickListener(this);
        btnOK.setOnClickListener(this);
        btnPartager.setOnClickListener(this);
    }

    /**
     * Update the view according to the player's information
     */
    private void updateViews() {
        intent = getIntent();
        pseudo = intent.getStringExtra(MainActivity.EXTRA_PSEUDO);

        etPseudo.setText(pseudo);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_resetScore:
                resetHighScore();
                break;
            case R.id.btn_OK:
                Intent intent_pseudo = new Intent(this, Game.class);
                if (!pseudo.equals(etPseudo.getText().toString())) {
                    pseudo = etPseudo.getText().toString();
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("username", pseudo);
                    editor.apply();
                    intent_pseudo.putExtra(MainActivity.EXTRA_PSEUDO, pseudo);
                }
                startActivity(intent_pseudo);
                break;
            case R.id.btn_Partager:
                shareHighScore();
                break;
        }
    }

    /**
     * Share the highestScore with intent
     */
    private void shareHighScore() {
        Intent share_intent = new Intent(Intent.ACTION_SEND);
        share_intent.setType("text/plain");
        share_intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.sharing_Score));
        share_intent.putExtra(Intent.EXTRA_TEXT, "Voici mon meilleur score: " + prefs.getLong("highScore", 0) + " ms");
        startActivity(share_intent);
    }

    /**
     * Reset to 0 the highest score of the player
     */
    private void resetHighScore() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("highScore");
        editor.apply();
        Intent intentReset = new Intent(this, Game.class);
        startActivity(intentReset);

    }
}