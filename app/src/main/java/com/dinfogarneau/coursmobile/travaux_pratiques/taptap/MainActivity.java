package com.dinfogarneau.coursmobile.travaux_pratiques.taptap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_PSEUDO = "com.dinfogarneau.coursmobile.travaux_pratiques." +
            "taptap.pseudo";
    EditText etPseudo;
    EditText etPassword;
    Button btLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
    }

    /**
     * Connecte le joueur au jeu si il entre les bons identifiants
     */
    public void connecterJoueur() {
        Intent intent = new Intent(this, Game.class);
        String pseudo = etPseudo.getText().toString();
        String password = etPassword.getText().toString();

        if (pseudo.equals(getString(R.string.pseudo_bot)) && password.equals(getString(R.string.password_123))) {
            intent.putExtra(EXTRA_PSEUDO, pseudo);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.login_error_message, Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews() {
        etPseudo = findViewById(R.id.et_Pseudo);
        etPassword = findViewById(R.id.et_Password);
        btLogin = findViewById(R.id.bt_Login);

        etPseudo.setAutofillHints(View.AUTOFILL_HINT_USERNAME);
        etPassword.setAutofillHints(View.AUTOFILL_HINT_PASSWORD);
        btLogin.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_Login:
                connecterJoueur();
        }
    }
}