package com.dinfogarneau.coursmobile.travaux_pratiques.taptap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class Game extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_SCORE = "com.dinfogarneau.coursmobile.travaux_pratiques." +
            "taptap.score";
    Button btnSettings;
    Button btnStart;
    TextView tvHighScore;
    TextView tvNomJoueur;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    String pseudo;
    SharedPreferences prefs;
    long score;
    private long startTime;
    private boolean bBtn1JustClicked;
    private boolean bBtn2JustClicked;
    private boolean bBtn3JustClicked;
    private boolean bBtn4JustClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        bindViews();
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE);
        updateViews();
        resetGame();

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("btn1Text", btn1.getText().toString());
        outState.putString("btn2Text", btn2.getText().toString());
        outState.putString("btn3Text", btn3.getText().toString());
        outState.putString("btn4Text", btn4.getText().toString());

        outState.putBoolean("btn1State", bBtn1JustClicked);
        outState.putBoolean("btn2State", bBtn2JustClicked);
        outState.putBoolean("btn3State", bBtn3JustClicked);
        outState.putBoolean("btn4State", bBtn4JustClicked);
        outState.putLong("startedTime", startTime);

        outState.putFloat("btn1Alpha", btn1.getAlpha());
        outState.putFloat("btn2Alpha", btn2.getAlpha());
        outState.putFloat("btn3Alpha", btn3.getAlpha());
        outState.putFloat("btn4Alpha", btn4.getAlpha());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        startTime = savedInstanceState.getLong("startedTime");
        btn1.setText(savedInstanceState.getString("btn1Text"));
        btn2.setText(savedInstanceState.getString("btn2Text"));
        btn3.setText(savedInstanceState.getString("btn3Text"));
        btn4.setText(savedInstanceState.getString("btn4Text"));
        btn1.setAlpha(savedInstanceState.getFloat("btn1Alpha"));
        btn2.setAlpha(savedInstanceState.getFloat("btn2Alpha"));
        btn3.setAlpha(savedInstanceState.getFloat("btn3Alpha"));
        btn4.setAlpha(savedInstanceState.getFloat("btn4Alpha"));
        bBtn1JustClicked = savedInstanceState.getBoolean("btn1State", false);
        bBtn2JustClicked = savedInstanceState.getBoolean("btn2State", false);
        bBtn3JustClicked = savedInstanceState.getBoolean("btn3State", false);
        bBtn4JustClicked = savedInstanceState.getBoolean("btn4State", false);
    }

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews() {
        btnSettings = findViewById(R.id.btn_Settings);
        btnStart = findViewById(R.id.btn_Start);
        tvHighScore = findViewById(R.id.tv_HighScore);
        tvNomJoueur = findViewById(R.id.tv_NomJoueur);
        btn1 = findViewById(R.id.btn_1);
        btn2 = findViewById(R.id.btn_2);
        btn3 = findViewById(R.id.btn_3);
        btn4 = findViewById(R.id.btn_4);

        btnSettings.setOnClickListener(this);
        btnStart.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Settings:
                Intent intent = new Intent(this, Configuration.class);
                intent.putExtra(MainActivity.EXTRA_PSEUDO, pseudo);
                intent.putExtra(EXTRA_SCORE, score);
                startActivity(intent);
                break;
            case R.id.btn_Start:
                resetGame();
                break;
            case R.id.btn_1:
                if (!bBtn1JustClicked && !bBtn4JustClicked) {
                    btn1.setAlpha(0.5f);
                    btn1.setText(R.string.button1_startedText);
                    startTime = System.currentTimeMillis();
                    bBtn1JustClicked = true;
                    bBtn4JustClicked = false;
                }
                break;
            case R.id.btn_2:
                if (bBtn1JustClicked && !bBtn4JustClicked) {
                    btn2.setAlpha(0.5f);
                    score = System.currentTimeMillis() - startTime;
                    btn2.setText(getString(R.string.txt_currentScore, score));
                    bBtn2JustClicked = true;
                    bBtn1JustClicked = false;
                }
                break;
            case R.id.btn_3:
                if (bBtn2JustClicked && !bBtn4JustClicked) {
                    btn3.setAlpha(0.5f);
                    score = System.currentTimeMillis() - startTime;
                    btn3.setText(getString(R.string.txt_currentScore, score));
                    bBtn3JustClicked = true;
                    bBtn2JustClicked = false;
                }
                break;
            case R.id.btn_4:
                if (bBtn3JustClicked && !bBtn4JustClicked) {
                    btn4.setAlpha(0.5f);
                    score = System.currentTimeMillis() - startTime;
                    btn4.setText(getString(R.string.txt_currentScore, score));
                    bBtn4JustClicked = true;
                    bBtn3JustClicked = false;
                    verifyScore(score);
                    break;
                }
        }
    }

    /**
     * Update the views according to the intent
     */
    private void updateViews() {
        pseudo = prefs.getString("username", "bot");

        tvNomJoueur.append(pseudo);
        updateHighScore();
    }

    /**
     * Verify is the score is higher than the highscore
     *
     * @param pScore The score of the player
     */
    private void verifyScore(long pScore) {
        if (pScore < prefs.getLong("highScore", 999999999)) {
            registerHighScore(pScore);
        }
        if (pScore >= 1000 && pScore <= 2000) {
            Intent intent = new Intent(this, Image.class);
            intent.putExtra(Game.EXTRA_SCORE, pScore);
            startActivity(intent);
        }
    }

    /**
     * Register Highscore in the sharedPreferences
     *
     * @param pScore The score of the player
     */
    private void registerHighScore(long pScore) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("highScore", pScore);
        editor.apply();
        updateHighScore();
    }

    /**
     * Update the highScore on the view
     */
    private void updateHighScore() {
        long highScore = prefs.getLong("highScore", 0);
        tvHighScore.setText(String.valueOf(highScore));
    }

    /**
     * Reset the game so every button return to normal state
     */
    private void resetGame() {

        resetButtonsState();
        resetButtonsText();
        resetButtonsColor();
    }

    /**
     * Reset the buttons's color to default value
     */
    private void resetButtonsColor() {
        btn1.setAlpha(1.0f);
        btn2.setAlpha(1.0f);
        btn3.setAlpha(1.0f);
        btn4.setAlpha(1.0f);
    }

    /**
     * Reset the buttons's text to default value
     */
    private void resetButtonsText() {
        btn1.setText(getString(R.string.button1_default));
        btn2.setText(getString(R.string.button2_default));
        btn3.setText(getString(R.string.button3_default));
        btn4.setText(getString(R.string.button4_default));
    }

    /**
     * Reset the boolean state of every buttons to default value
     */
    private void resetButtonsState() {
        bBtn1JustClicked = false;
        bBtn2JustClicked = false;
        bBtn3JustClicked = false;
        bBtn4JustClicked = false;
    }
}